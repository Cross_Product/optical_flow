function processedpatches = preprocessFolder(folderpath,k,p)
% INPUT: folderpath = path to the top-level folder in the Sintel dataset.
%        k = k for k-th nearest neighbor search.
%        p = percent of densest points to use for core subset.
% OUTPUT: a matrix of preprocessed image patches. Each row is a single patch.

% This code is written for the dataset accompanying the paper http://files.is.tue.mpg.de/black/papers/ButlerECCV2012-corrected.pdf
% To run this code, all of the .flo files need to be in the same folder (and hence with different names) --- they need to be copied from the flow/alley_1 and flow/alley_2 (etc) folders into the same folder.
% To get different names, the terminal script "for i in *.flo; do mv "$i" "${i/.flo/_alley_1.flo}"; done" is helpful.
% From there, copy and paste the .flo files into the same folder.

NUM_PATCHES = 385; 
PATCH_SIZE = 3;   % We always use 3x3 image patches.

% Process the data in all of the folders.
folders = dir(folderpath);
directoryflags = [folders.isdir];
subfolders = folders(directoryflags);
numfolders = length(subfolders);
allpatches = [];
for j = 1:length(subfolders)
   tempdir = [folderpath,subfolders(j).name,'/'];
   % Get a list of all .flo files in the directory specified and process them:
   filelist = dir([tempdir,'*.flo']);
   [numfiles,~] = size(filelist);
   display('Number of files to process');
   display(numfiles);
   folderpatches = zeros(NUM_PATCHES*numfiles,2*PATCH_SIZE^2+1);
   % Sample from every .flo file in the subdirectory.
   for i = 1:numfiles
      filepath = [tempdir,filelist(i).name];
      patches = sampleFromDataSet(filepath,NUM_PATCHES,PATCH_SIZE);
      patches = computeContrastNorm(patches);
      folderpatches((i-1)*NUM_PATCHES+1:i*NUM_PATCHES,:) = patches;
   end
   allpatches = [allpatches;folderpatches];
end

% Do this processing on the entire folder of data
HCpatches = getHighContrastPatches(allpatches);
HCpatches = randomDownsample(HCpatches,50000);
HCpatches = normalizePatches(HCpatches);
[~,n] = size(HCpatches);
HCpatches = HCpatches(:,1:n-1);
processedpatches = kNNDensityThreshold(HCpatches,k,p);
