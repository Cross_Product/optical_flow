function processedpatches = partialPreprocessFolder(folderpath)
% INPUT: path to a folder containing .flo files from the MPI Sintel data set.
% OUTPUT: an n*18 matrix of high contrast optical flow patches. Each row is a 
%        single data point, where the first nine entries are the horizontal and
%        second nine entries are the vertical components of optical flow.
% NOTE:  We apply only part of the preprocessing -- files are read from the
%        data set, the 20% highest-contrast data is selected.

NUM_PATCHES = 385; % Chosen so that when sampling from all 1041 frames in the
%                    Sintel data set we get about 400,000 total data points.
PATCH_SIZE = 3;   % We always use 3x3 image patches.
DOWNSAMPLE_SIZE = 50000;   % In order to match the writeup.

% Get a list of all .flo files in the directory specified and process them:
filelist = dir([folderpath,'*.flo'])
[numfiles,~] = size(filelist)

display('Number of files to process');
display(numfiles);

allpatches = zeros(NUM_PATCHES*numfiles,2*PATCH_SIZE^2+1);

for i = 1:numfiles
   filepath = [folderpath,filelist(i).name];
   patches = sampleFromDataSet(filepath,NUM_PATCHES,PATCH_SIZE);
   patches = computeContrastNorm(patches);
   allpatches((i-1)*NUM_PATCHES+1:i*NUM_PATCHES,:) = patches;
end

% Do this processing on the entire folder of data
HCpatches = getHighContrastPatches(allpatches);
HCpatches = randomDownsample(HCpatches,DOWNSAMPLE_SIZE);

% Remove the contrast norm column
processedpatches = HCpatches(:,1:2*PATCH_SIZE^2);
