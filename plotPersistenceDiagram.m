function plotPersistenceDiagram(persistence)
% INPUT: A list of persistence intervals, in the form of an n*2 vector, where
%        the first column is the birth time and second column is the death time.
% OUTPUT: a figure containing the persistence diagram.

figure;
hold on;
xmax = max(persistence(:));
plot([0,xmax],[0,xmax]);
plot(persistence(:,1),persistence(:,2),'*');
axis([0 inf 0 inf]);
grid on;
