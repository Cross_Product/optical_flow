% Must run load_javaplex while in the matlab examples folder first.
import edu.stanford.math.plex4.*;
% You must have installed Javaplex to use this file.

folderpath = '../MPI-Sintel-complete/training/flow/';
% This folderpath works if the folder MPI-Sintel-complete is one level 
% higher than the bitbucket folder optical_flow in which this file resides.
numAngles = 12;
k = 300;
p = 50;
numSequential = 50; % Number of patches to choose using sequential maxmin
DOWNSAMPLE_SIZE = 50000;

%disp('Getting data from files.')
%patches = preprocessFolderWithAnglesAll(folderpath,k,p);
temp = load('patches_complete');
patches = temp.patches;

for i=0:numAngles-1
    disp(['Processing angle ',int2str(i+1),' out of ',int2str(numAngles)])
    theta1 = (i-1)*pi/numAngles;
    theta2 = (i+1)*pi/numAngles;
    anglepatches = getDataInAngleRange(patches,theta1,theta2);
    % Remove the angle from the data structure:
    anglepatches = anglepatches(:,1:19);
    % Tell us how many data points are in this bin:
    display(['After selecting the angle range, the number of patches is: ',int2str(size(anglepatches,1))]);
    HCpatches = getHighContrastPatches(anglepatches);
    [m,~] = size(HCpatches);
    if m > DOWNSAMPLE_SIZE
        HCpatches = randomDownsample(HCpatches,DOWNSAMPLE_SIZE);
        disp(['Downsampling to ',int2str(DOWNSAMPLE_SIZE),' high-contrast patches']);
    else
        disp(['Only ',int2str(m),' high-contrast patches, so skipping the downsampling.']);
    end
    HCpatches = normalizePatches(HCpatches);
    [~,n] = size(HCpatches);
    HCpatches = HCpatches(:,1:n-1);
    processedpatches = kNNDensityThreshold(HCpatches,k,p);
    % Do sequential max-min downsampling
    maxmin_selector = api.Plex4.createMaxMinSelector(processedpatches, numSequential);
    patchesSequentialMaxmin = processedpatches(maxmin_selector.getLandmarkPoints() + 1, :);
    filename=['data/angle',int2str(i),'k',int2str(k),'p',int2str(p),'seqmaxmin',int2str(numSequential),'.csv'];
    csvwrite(filename,patchesSequentialMaxmin);
    plotDctFlowCombo(processedpatches,cos(i*pi/numAngles),1,9,cos(i*pi/numAngles),2,10)
    plotDctFlowCombo(patchesSequentialMaxmin,cos(i*pi/numAngles),1,9,cos(i*pi/numAngles),2,10)
    %title(filename)
    %savefig(filename)
    %saveas(gcf,[filename,'_proj'],'png')
end
