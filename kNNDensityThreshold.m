function densestpoints = kNNDensityThreshold(data,k,p)
% INPUT: a matrix of data (with contrast norm 1) in which each ROW is an data
%        point. (There should be no extra row with the norms included.)
%        k = the k for k-th nearest neighbor.
%        p = percentage of densest points to keep
% OUTPUT: the subset of the input data consisting of the p% densest points, as
%        determined by kNN search. This data is formatted in the same way as
%        the input matrix.
% NOTE: this is referred to as X(k,p) in the paper.

% Initialize a KD Tree object from the matrix, using Euclidean norm.
modelKDT = KDTreeSearcher(data);
% Perform a kNN search on X. The k nearest neighbors of x in X always include
%        the point x itself, so index with k+1 to actually get kNN.
[indices,distances] = knnsearch(modelKDT,data,'K',k+1);

% Create a table with first column = index in data and second column = distance
%        to k=th nearest neighbor.
distancetable = [indices(:,1),distances(:,k+1)];
% Sort by distance to kNN (least to greatest).
distancetable = sortrows(distancetable,2);

% Keep only the p% densest points, which are the points with the shortest
%        distance to their kNN.
[m,n] = size(data);            % m is number of data points, n is vector size.
numdatapoints = m*p/100;                  % number of points to keep.
intnumdatapoints= floor(numdatapoints);
if numdatapoints ~= intnumdatapoints
   warning('%d percent of %d points is not an integer. Taking the %d densest points',p,m,intnumdatapoints);
end

densestpointsindex = distancetable(1:intnumdatapoints,1);
densestpoints = zeros(intnumdatapoints,n);
for i = 1:intnumdatapoints
   densestpoints(i,:) = data(densestpointsindex(i),:);
end
