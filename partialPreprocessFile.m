function processedpatches = partialPreprocessFile(filepath)
% INPUT: path to a .flo file from the MPI Sintel data set.
% OUTPUT: an n*18 matrix of high contrast optical flow patches. Each row is a 
%        single data point, where the first nine entries are the horizontal and
%        second nine entries are the vertical components of optical flow.
% NOTE:  We apply only part of the preprocessing -- files are read from the
%        data set, the 20% highest-contrast data is selected.
NUM_PATCHES = 385; % Chosen so that when sampling from all 1041 frames in the
%                    Sintel data set we get about 400,000 total data points.
PATCH_SIZE = 3;   % We always use 3x3 image patches.
DOWNSAMPLE_SIZE = 50000;
% Step 1: sample from image.
data = sampleFromDataSet(filepath,NUM_PATCHES,PATCH_SIZE);
% Step 2: compute contrast norm.
data = computeContrastNorm(data);
% Step 3: get only high contrast patches.
HCdata = getHighContrastPatches(data);
% Remove the contrast norm data from the matrix.
processedpatches = HCdata(:,1:2*PATCH_SIZE^2);
