function mtx = computeContrastNorm(patches)
% INPUT: patches, an m*n matrix whose rows are vectors of optical flow. The
%        first n/2 entries of each row are assumed to be the horizontal
%        component, and the last n/2 the vertical component.
% OUTPUT: mtx, a n*(m+1) matrix with the contrast norm of each vector in the
%        last entry of each row.

% Build the correct-size D-norm matrix.
[mrows,ncols] = size(patches);
n = sqrt(ncols/2);
% Validate matrix size.
if n ~= floor(n)
   error(['Improperly sized patch data matrix. Number of columns should be '...
      '2*n^2 for some integer n.']);
end
D = getDNormMatrix(n);

% Compute the contrast D norm and add it as a row on the bottom of the patch
%        data matrix.
mtx = [patches, zeros(mrows,1)];
newcolnumber = ncols+1;
for i = 1:mrows
   u = patches(i,1:n^2);
   v = patches(i,n^2+1:ncols);
   d = sqrt(u*D*u' + v*D*v');
   mtx(i,newcolnumber) = d;
end
