function plotDctFlowCombo(points,ci1,i1,i2,cj1,j1,j2)
 
% INPUT:
%   points - N x 18 matrix of optical flow patches
%   i1, i2, j1, j2 - element of {1,...,16}
%   ci1, cj1 - real number between 0 and 1
%
% OUTPUT:
%   This function converts points to the DCT basis and plots the projection
%   onto basis vectors ci1*i1+(1-ci1^2)*i2 (x axis) and cj1*j1+(1-cj1^2)*j2
%   (y axis). 
%   If i1 is in {1,...,8}, then the basis vector is e_i1^u, and similarly
%   for i2, j1, j2.
%   If i1 is in {9,...,16}, then the basis vector is e_{i1-8}^v, and 
%   similarly for i2, j1, j2.
%   See dctFlow.m for more info on change of coordinates

% Setup the DCT basis:
e1=1/sqrt(6)*[1;0;-1;1;0;-1;1;0;-1];
e2=1/sqrt(6)*[1;1;1;0;0;0;-1;-1;-1];
e3=1/sqrt(54)*[1;-2;1;1;-2;1;1;-2;1];
e4=1/sqrt(54)*[1;1;1;-2;-2;-2;1;1;1];
e5=1/sqrt(8)*[1;0;-1;0;0;0;-1;0;1];
e6=1/sqrt(48)*[1;0;-1;-2;0;2;1;0;-1];
e7=1/sqrt(48)*[1;-2;1;0;0;0;-1;2;-1];
e8=1/sqrt(216)*[1;-2;1;-2;4;-2;1;-2;1];
lambda=sparse(diag([1,1,3,3,2,4,4,6],0));
A=[e1,e2,e3,e4,e5,e6,e7,e8];
AA=[A,zeros(9,8);zeros(9,8),A];
lambdalambda=[lambda,sparse(8,8);sparse(8,8),lambda];

% Make a plot
pointsDct=points*AA*lambdalambda';
figure;
plot(ci1*pointsDct(:,i1)+sqrt(1-ci1^2)*pointsDct(:,i2),cj1*pointsDct(:,j1)+sqrt(1-cj1^2)*pointsDct(:,j2),'.'), axis square %axis equal