function theta = computeAngles(patches)
% INPUT: An n by k matrix of patches, where k is the length of the data, 
%       assumed to be at least 18.
% OUTPUT: the angle theta associated to each data point as a column vector 
% with the same number of rows as the original matrix.

[mrows,ncols] = size(patches);
if ncols < 18
   error('Data matrix has fewer than the expected 18 columns');
end

% Compute the x and y direction vectors of each data point in the plane of 
% best fit as determined by PCA.
xy = zeros(mrows,2);
for i = 1:mrows
   vec = patches(i,1:18);
   xy(i,:) = getAnglesWithPCA(vec);
end

theta = atan2(xy(:,2),xy(:,1));
theta = mod(theta,pi);
