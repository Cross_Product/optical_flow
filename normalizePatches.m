function patches = normalizePatches(patches)
% INPUT: A matrix of optical flowdata patches whose last row is contrast 
%       norm.
% OUTPUT: The same matrix with entries normalized to have zero average flow in
%       both horizontal and vertical directions, and contrast norm 1.

[mrows,ncols] = size(patches);
vecsize = (ncols-1)/2;
% Verify size:
if vecsize ~= floor(vecsize)
   error('Patch data matrix should have an odd number of rows.');
end

% Compute averages of both horizontal and vertical flow components.
uavg = zeros(mrows,1);
vavg = zeros(mrows,1);
for i = 1:mrows
   uavg(i) = mean(patches(i,1:vecsize));
   vavg(i) = mean(patches(i,vecsize+1:ncols-1));
end

% "Mean center" both horizontal and vertical components.
patches(:,1:vecsize) = patches(:,1:vecsize) - uavg;
patches(:,1+vecsize:ncols-1) = patches(:,1+vecsize:ncols-1) - vavg;

% Do the normalization.
% Confirm that we never divide by zero:
zerotest = find(patches(:,ncols)<10^(-10));
if ~isempty(zerotest)
   warning('One of these vectors has norm very close to zero!');
   warning('The affected vectors are in rows:');
   display(zerotest);
end

% Normalize.
for i = 1:mrows
   patches(i,1:ncols-1) = patches(i,1:ncols-1)/patches(i,ncols);
end
