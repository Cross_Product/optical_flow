# Optical_Flow

The code in this repository accompanies our paper "On the nonlinear statistics of optical flow" by Henry Adams, Johnathan Bush, Brittany Carr, Lara Kassab, and Joshua Mirth, to appear in Pattern Recognition Letters, special issue Topological Analysis and Recognition (2018+). The code required to reproduce the computations and figures therein is included here.

# Getting Started

Dependencies:

- Matlab, with the Statistic and Machine Learning Toolbox.

- Dionysus, available at <http://mrzv.org/software/dionysus2/index.html>.

- Python 3.x, with the [NumPy](http://www.numpy.org/) library.

- The [Sintel data set](http://sintel.is.tue.mpg.de/).

# Replicating the computations

TODO: describe the steps required to do this.
