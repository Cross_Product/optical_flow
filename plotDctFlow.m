function plotDctFlow(points,i,j)

% INPUT:
%   points - N x 18 matrix of optical flow patches
%   i - element of {1,...,16}
%   j - element of {1,...,16}
%
% OUTPUT:
%   This function converts points to the DCT basis and plots the projection
%   onto basis vectors i (x axis) and j (y axis). 
%   If i is in {1,...,8}, then the i-th basis vector is e_i^u.
%   If i is in {9,...,16}, then the i-th basis vector is e_{i-8}^v.
%   See dctFlow.m for more info on change of coordinates

% Setup the DCT basis:
e1=1/sqrt(6)*[1;0;-1;1;0;-1;1;0;-1];
e2=1/sqrt(6)*[1;1;1;0;0;0;-1;-1;-1];
e3=1/sqrt(54)*[1;-2;1;1;-2;1;1;-2;1];
e4=1/sqrt(54)*[1;1;1;-2;-2;-2;1;1;1];
e5=1/sqrt(8)*[1;0;-1;0;0;0;-1;0;1];
e6=1/sqrt(48)*[1;0;-1;-2;0;2;1;0;-1];
e7=1/sqrt(48)*[1;-2;1;0;0;0;-1;2;-1];
e8=1/sqrt(216)*[1;-2;1;-2;4;-2;1;-2;1];
lambda=sparse(diag([1,1,3,3,2,4,4,6],0));
A=[e1,e2,e3,e4,e5,e6,e7,e8];
AA=[A,zeros(9,8);zeros(9,8),A];
lambdalambda=[lambda,sparse(8,8);sparse(8,8),lambda];

% Make a plot
pointsDct=points*AA*lambdalambda';
plot(pointsDct(:,i),pointsDct(:,j),'.'), axis square %axis equal