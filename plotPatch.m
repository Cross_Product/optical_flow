function plotPatch(patch)

% Plots a flow vector of length 18


% Plots the patch as a vector field
figure;
quiver(flipud(reshape(patch(1:9),3,3)),flipud(reshape(patch(10:18),3,3)),.5)
%imagesc(flipud([reshape(patch(1:9),3,3),reshape(patch(10:18),3,3)]))


% Plots the patch as two 3x3 matrices
%colormap(gray)
%imagesc(reshape(patch,3,6))

% i=i+500; plotPatch(allpatches(i,:))
