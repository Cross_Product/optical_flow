function patches = sampleFromDataSet(filepath,numpatches,patchsize)
% INPUT: filepath = a path to a .flo file in the MPI Sintel database.
%        numpatches = number of patches to sample.
%        patchsize = size of patches (n by n).
% OUTPUT: [numpatches] vectors of length 2[patchsize]^2. The first n elements 
%        are the horizontal and the second n elements are the vertical
%        components of the flow. These vectors are stored as the columns of a
%        matrix of size [numpatches] by 2[patchsize]^2.
% RECOMMENDED: use n = 3.

% Load the tensor of data, using readFlowFile().
data = readFlowFile(filepath);
[m,n,~] = size(data);

% Sanity checks.
if numpatches < 1
   error('Number of patches needs to be positive.');
end
if patchsize < 1
   error('Patch size must be positive.');
end
if or(patchsize > m,patchsize > n)
   error('Patch size must be smaller than data matrix');
end

% Sample a random set of points by specifying the upper-left coordinates.
% NOTE: strictly speaking there is nothing to prevent this from sampling the
%        same patch twice.
patchrows = randi([1,m-patchsize+1],1,numpatches);
patchcols = randi([1,n-patchsize+1],1,numpatches);

% Loop through the points collecting the patches
patches = zeros(2*patchsize^2,numpatches);
for i = 1:numpatches
   p = data(patchrows(i):patchrows(i)+patchsize-1,patchcols(i):patchcols(i)+patchsize-1,:);
   p = reshape(p,[2*patchsize*patchsize,1]);
   patches(:,i) = p;
end

% Transpose so that data are in rows
patches = patches';
