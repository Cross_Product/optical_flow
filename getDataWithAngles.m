function anglepatches = getDataWithAngles(folderpath)
% INPUT: folderpath = path to a folder of .flo files to be processed.
%        k = k for k-th nearest neighbor search.
%        p = percent of densest points to use for core subset.
% OUTPUT: a matrix of preprocessed image patches. Each row is a single patch.
%        The last column is the angle computed via PCA and the second-to-last
%        column is the contrast norm.

NUM_PATCHES = 385; 
PATCH_SIZE = 3;   % We always use 3x3 image patches.

% Get a list of all .flo files in the directory specified and process them:
filelist = dir([folderpath,'*.flo']);
[numfiles,~] = size(filelist);

display('Number of files to process');
display(numfiles);

allpatches = zeros(NUM_PATCHES*numfiles,2*PATCH_SIZE^2+1);

for i = 1:numfiles
   filepath = [folderpath,filelist(i).name];
   patches = sampleFromDataSet(filepath,NUM_PATCHES,PATCH_SIZE);
   patches = computeContrastNorm(patches);
   allpatches((i-1)*NUM_PATCHES+1:i*NUM_PATCHES,:) = patches;
end

% Do this processing on the entire folder of data
angles = computeAngles(allpatches);
anglepatches = [allpatches,angles];
