function D = getDNormMatrix(patchSize)

% This function produces the D-norm matrix, which is used to compute the
% contrast of a square patch. See page 88 of "The Nonlinear Statistics of
% High-Contrast Patches in Natural Images" by A.B. Lee, K.S. Pedersen, and 
% D. Mumford. Page 88 contains the D-norm matrix for 3x3 matrices, which
% can be reproduced by calling this function with patchSize = 3.
%
% INPUT:
%   patchSize - the width (equivalently height) of the square patches. For
%       example, set patchSize = 3 if you want the D-norm matrix for 3x3
%       patches.
%
% OUTPUT:
%   D - the D-norm matrix for computing the contrast of a square 
%       patchSize x patchSize patch.
%
% TEST:
%   DnormMatrix(3) should be equal to [2,-1,0,-1,0,0,0,0,0;-1,3,-1,0,-1,0,0,0,0;0,-1,2,0,0,-1,0,0,0;-1,0,0,3,-1,0,-1,0,0;0,-1,0,-1,4,-1,0,-1,0;0,0,-1,0,-1,3,0,0,-1;0,0,0,-1,0,0,2,-1,0;0,0,0,0,-1,0,-1,3,-1;0,0,0,0,0,-1,0,-1,2]

D = zeros(patchSize^2,patchSize^2);
for i = 1:patchSize^2 % loop over rows of D
    count = 0;
    if i > patchSize
        D(i,i-patchSize) = -1;
        count = count+1;
    end
    if mod(i,patchSize) ~= 1
        D(i,i-1) = -1;
        count = count+1;
    end
    if mod(i,patchSize) ~= 0
        D(i,i+1) = -1;
        count = count+1;
    end
    if i <= patchSize^2-patchSize
        D(i,i+patchSize) = -1;
        count = count+1;
    end
    D(i,i) = count;    
end
