function highcontrastpatches = getHighContrastPatches(patches)
% INPUT: A matrix of data patches whose last row is contrast norm.
% OUTPUT: A submatrix containing only the columns of the patches whose 
%        contrast norm in the top fraction.

FRACTION = 1/5; % Fraction of patches to keep as "high-contrast" patches

[mrows,ncols] = size(patches);
numberhighcontrast = ceil(mrows*FRACTION); % Ceiling guarantees at least one.

% Sort the rows by their contrast norm (the last entry).
patches = sortrows(patches,ncols);
highcontrastpatches = patches(mrows-numberhighcontrast+1:mrows,:);
