function directionVector = getAnglesWithPCA(patch)
% INPUT: 18x1 vector encoding a 3x3 optical flow patch
% OUTPUT: 2x1 dominant flow direction

%Construct X a 9x2 matrix, where the i-th row of X is (h_i , v_i), and i 
% varies from 1 to 9
X = zeros(9,2);
for i = 1:9
    X(i,1) = patch(i);
    X(i,2) = patch(i+9);
end

% Each column of coeff contains coefficients for one principal component, 
% and the columns are in descending order of component variance. 
% By default, pca centers the data and uses the singular value 
% decomposition (SVD) algorithm.
coeff = pca(X);
directionVector = coeff(:,1);
end


