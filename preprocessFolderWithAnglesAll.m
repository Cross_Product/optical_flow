function processedpatches = preprocessFolderWithAnglesAll(folderpath,k,p)
% INPUT: folderpath = path to a folder of .flo files to be processed.
%        k = k for k-th nearest neighbor search.
%        p = percent of densest points to use for core subset.
% OUTPUT: a matrix of preprocessed image patches. Each row is a single patch. These contains both contrast norm and angle.

NUM_PATCHES = 385; 
PATCH_SIZE = 3;   % We always use 3x3 image patches.
DOWNSAMPLE_SIZE = 50000;

% Process the data in all of the folders.
folders = dir(folderpath);
directoryflags = [folders.isdir];
subfolders = folders(directoryflags);
allpatches = [];
for j = 1:length(subfolders)
   tempdir = [folderpath,subfolders(j).name,'/'];
   % Get a list of all .flo files in the directory specified and process them:
   filelist = dir([tempdir,'*.flo']);
   [numfiles,~] = size(filelist);
   %disp(['Number of files to process in folder ',subfolders(j).name,': ',int2str(numfiles)]);
   folderpatches = zeros(NUM_PATCHES*numfiles,2*PATCH_SIZE^2+1);
   % Sample from every .flo file in the subdirectory.
   for i = 1:numfiles
      filepath = [tempdir,filelist(i).name];
      patches = sampleFromDataSet(filepath,NUM_PATCHES,PATCH_SIZE);
      patches = computeContrastNorm(patches);
      folderpatches((i-1)*NUM_PATCHES+1:i*NUM_PATCHES,:) = patches;
   end
   allpatches = [allpatches;folderpatches];
end
% Do this processing on the entire folder of data
angles = computeAngles(allpatches);
processedpatches = [allpatches,angles];

%OLD stuff:
%anglepatches = getDataInAngleRange([allpatches,angles],theta1,theta2);
% Remove the angle from the data structure:
%anglepatches = anglepatches(:,1:19);
% Tell us how many data points are in this bin:
% display(['After selecting the angle range, the number of patches is: ',int2str(size(anglepatches,1))]);

%HCpatches = getHighContrastPatches(anglepatches);
%[m,~] = size(HCpatches);
%if m > DOWNSAMPLE_SIZE
%   HCpatches = randomDownsample(HCpatches,DOWNSAMPLE_SIZE);
%   disp(['Downsampling to ',int2str(DOWNSAMPLE_SIZE),' high-contrast patches']);
%else
%   disp(['Only ',int2str(m),' high-contrast patches, so skipping the downsampling.']);
%end
%HCpatches = normalizePatches(HCpatches);
%[~,n] = size(HCpatches);
%HCpatches = HCpatches(:,1:n-1);
%processedpatches = kNNDensityThreshold(HCpatches,k,p);
