function processedpatches = preprocessFile(filepath,k,p)
% INPUT: filepath = pathname of .flo file to be processed.
%        k = k for k-th nearest neighbor search.
%        p = percent of densest points to use for core subset.
% OUTPUT: a matrix of preprocessed image patches. Each row is a single patch.
NUM_PATCHES = 385; % Chosen so that when sampling from all 1041 frames in the
%                    Sintel data set we get about 400,000 total data points.
PATCH_SIZE = 3;   % We always use 3x3 image patches.
% Step 1: sample from image.
patches = sampleFromDataSet(filepath,NUM_PATCHES,PATCH_SIZE);
% Step 2: compute contrast norm.
patches = computeContrastNorm(patches);
% Step 3: get only high contrast patches.
HCpatches = getHighContrastPatches(patches);
% Step 4: randomly downsample to desired size (skipped).
% Step 5: normalize data.
HCpatches = normalizePatches(HCpatches);
% Step 6: filter by denstiy.
% Remove the contrast norm from the data.
[~,n] = size(HCpatches);
HCpatches = HCpatches(:,1:n-1);
processedpatches = kNNDensityThreshold(HCpatches,k,p);
