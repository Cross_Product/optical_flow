function anglepatches = getDataInAngleRange(anglepatches,theta1,theta2)
% INPUT: matrix of data where the last column is the angle of the data point.
%        theta1,theta2 = range of angles to find data between.
% OUTPUT: subset of the data matrix with angle between theta1 and theta2.

[~,ncols] = size(anglepatches);

if theta1 > theta2
   patchindices = find(anglepatches(:,ncols) > theta1 | anglepatches(:,ncols) < theta2);
else
   patchindices = find(anglepatches(:,ncols) > theta1 & anglepatches(:,ncols) < theta2);
end
anglepatches = anglepatches(patchindices,:);
