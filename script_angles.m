folderpath = '../MPI-Sintel-complete/training/flow/';
% This folderpath works if the folder MPI-Sintel-complete is one level 
% higher than the bitbucket folder optical_flow in which this file resides.
numAngles = 12;
k = 300;
p = 50;
allpatches = [];

for i=0:numAngles-1
    disp(['Processing angle ',int2str(i),' out of ',int2str(numAngles)])
    patches=preprocessFolderWithAngles(folderpath,k,p,(i-1)*pi/numAngles,(i+1)*pi/numAngles);
    allpatches = [allpatches;patches];
    filename=['angle',int2str(i),'k',int2str(k),'p',int2str(p)];
    save(filename,'patches')
    plotDctFlowCombo(patches,cos(i*pi/numAngles),1,9,cos(i*pi/numAngles),2,10)
    title(filename)
    savefig(filename)
    saveas(gcf,[filename,'_proj'],'png')
end
save(['allpatches_k',int2str(k),'p',int2str(p)],'allpatches');
