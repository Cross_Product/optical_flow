function patches = randomDownsample(data,numpatches)
% INPUT: matrix of image patches, where each row is a data point.
%        numpatches = size of downsampled data
% OUTPUT: a random p% subset of the data.

[mrows,ncols] = size(data);
if numpatches > mrows
   error('Input matrix smaller than desired downsample size.');
end

pointlist = 1:mrows;
sampledpoints = randsample(pointlist,numpatches);
patches = data(sampledpoints,:);